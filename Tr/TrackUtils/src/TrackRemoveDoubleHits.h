#ifndef _TrackRemoveDoubleHits_H_
#define _TrackRemoveDoubleHits_H_

/** @class TrackRemoveDoubleHits TrackRemoveDoubleHits Tr/TrackUtils/TrackRemoveDoubleHits.h
 *
 *  Removes Double hits in TT or IT ladders or in OT modules
 *
 *  @author L. Nicolas
 *  @date   14/09/2007
 */

//===========================================================================
// Includes
//===========================================================================
// Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

// Event
#include "Event/Track.h"
#include "Event/Node.h"
#include "Event/STCluster.h"
#include "Event/STLiteCluster.h"
#include "Event/OTTime.h"
//===========================================================================

class TrackRemoveDoubleHits: public GaudiAlgorithm {

public:

  // Constructor
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode initialize() override;
  StatusCode execute() override;
  StatusCode finalize() override;

private:

  bool isHighThreshold ( const LHCb::LHCbID& theLHCbID );
  double charge ( const LHCb::LHCbID& theLHCbID );

  int m_nTTHits = 0;
  int m_nITHits = 0;
  int m_nOTHits = 0;


  //======================================================================
  typedef LHCb::STLiteCluster::STLiteClusters STLiteClusters;

  const LHCb::Tracks* m_tracks = nullptr;
  // Location of the tracks container
  Gaudi::Property<std::string> m_tracksPath{ this, "TracksLocation", "Rec/Track/Best" };

  // Do we want to keep the strip with the highest charge of a double hit?
  // OR
  // Do we want to keep the strip with a high threshold of a double hit?
  Gaudi::Property<bool> m_keepHighThreshold{ this, "KeepHighThreshold", false };
  Gaudi::Property<bool> m_keepHighCharge{ this, "KeepHighCharge", false };

  // Location of the cluster containers
  // TT stuff
  Gaudi::Property<std::string> m_ttLiteClustersPath{ this, "TTLiteClustersLocation", LHCb::STLiteClusterLocation::TTClusters };
  const STLiteClusters* m_ttLiteClusters = nullptr;
  Gaudi::Property<std::string> m_ttClustersPath{ this, "TTClustersLocation", LHCb::STClusterLocation::TTClusters };
  const LHCb::STCluster::Container* m_ttClusters = nullptr;

  // IT stuff
  Gaudi::Property<std::string> m_itLiteClustersPath{ this, "ITLiteClustersLocation", LHCb::STLiteClusterLocation::ITClusters };
  const STLiteClusters* m_itLiteClusters = nullptr;
  Gaudi::Property<std::string> m_itClustersPath{ this, "ITClustersLocation", LHCb::STClusterLocation::ITClusters };
  const LHCb::STCluster::Container* m_itClusters = nullptr;

  //======================================================================

};

#endif // _TrackRemoveDoubleHits_H_
