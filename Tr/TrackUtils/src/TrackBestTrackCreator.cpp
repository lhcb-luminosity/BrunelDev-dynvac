#include "TrackBestTrackCreator.h"

/** @file TrackBestTrackCreator.cpp
 *
 * @autor Wouter Hulsbergen
 *
 * @author Manuel Schiller
 * @date 2014-11-18
 */

//-----------------------------------------------------------------------------
// Implementation file for class : TrackBestTrackCreator
//
// 2011-12-01 Wouter Hulsbergen
//-----------------------------------------------------------------------------

DECLARE_ALGORITHM_FACTORY( TrackBestTrackCreator )

TrackBestTrackCreator::TrackDataGen::TrackDataGen(TrackBestTrackCreator& parent, CopyMap& copymap, const std::string& fitterTypeAndName) :
m_parent(parent), m_curcont(m_parent.m_tracksInContainers.begin()),
  m_currange(), m_currangeit(m_currange.end()),
  m_nrtracks(0), m_copymap(copymap),
  m_fitterTypeAndName(fitterTypeAndName)
{ }

TrackBestTrackCreator::TrackDataGen::~TrackDataGen()
{
  // sort m_copymap for quick lookup
  std::sort(std::begin(m_copymap), std::end(m_copymap),
            [] (const CopyMapEntry& a, const CopyMapEntry& b)
            { return a.first < b.first; });
}

TrackData TrackBestTrackCreator::TrackDataGen::operator()()
{
  while (true) {
    if (m_currangeit != m_currange.end()) {
      // not at end of current container, so get next track
      const LHCb::Track* oldtr = *m_currangeit++;
      // clone track
      LHCb::Track* tp;
      std::string prefix ("TrackVectorFitter");
      if (!m_fitterTypeAndName.compare(0, prefix.size(), prefix)) {
        tp = new Tr::TrackVectorFit::FitTrack();
      } else {
        tp = new LHCb::Track();
      }
      tp->copy(*oldtr);
      std::unique_ptr<LHCb::Track> tr {tp};

      // pre-initialise (if required)
      const bool fitted = m_parent.m_doNotRefit && ( tr->fitStatus() == LHCb::Track::FitStatus::Fitted ||
                                                     tr->fitStatus() == LHCb::Track::FitStatus::FitFailed );
      if (fitted && tr->fitStatus() == LHCb::Track::FitStatus::FitFailed )
        continue;
      StatusCode sc(m_parent.m_initTrackStates  && !fitted ?
                    m_parent.m_stateinittool->fit(*tr, true) : StatusCode::SUCCESS);
      // if successful, return new TrackData object
      if (sc.isSuccess()) {
        if (m_parent.m_useAncestorInfo) {
          // save mapping between original track and its copy
          m_copymap.emplace_back(oldtr, m_nrtracks);
        }
        ++m_nrtracks;
        // keep a record where this track came from
        tr->addToAncestors(oldtr);
        return TrackData(std::move(tr));
      }
      // report error in log file
      m_parent.Warning("TrackStateInitTool fit failed", sc, 0 ).ignore();
    } else {
      // need to start with new container
      if (m_parent.m_tracksInContainers.end() == m_curcont) {
        // all tracks read from all containers, nothing to do any more
        throw DataExhaustedException("No more tracks.");
      }
      m_currange = m_parent.get<LHCb::Track::Range>(*m_curcont++);
      m_currangeit = m_currange.begin();
    }
    // move on to next track
  }
}

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TrackBestTrackCreator::TrackBestTrackCreator( const std::string& name,
						    ISvcLocator* pSvcLocator) :
#if DEBUGHISTOGRAMS
  GaudiHistoAlg ( name , pSvcLocator ),
#else
  GaudiAlgorithm ( name , pSvcLocator ),
#endif
  m_stateinittool("TrackStateInitTool",this),
  m_fitter("TrackMasterFitter",this),
  m_ghostTool("Run2GhostId",this),
  m_nGhosts(0)
{
  // default list of input containers
  m_tracksInContainers = { LHCb::TrackLocation::Forward,
                           LHCb::TrackLocation::Match,
                           LHCb::TrackLocation::VeloTT,
                           LHCb::TrackLocation::Downstream,
                           LHCb::TrackLocation::Tsa,
                           LHCb::TrackLocation::Velo };

  declareProperty( "TracksInContainers",m_tracksInContainers );
  declareProperty( "TracksOutContainer", m_tracksOutContainer = LHCb::TrackLocation::Default );
  declareProperty( "MaxOverlapFracVelo", m_maxOverlapFracVelo = 0.5);
  declareProperty( "MaxOverlapFracT", m_maxOverlapFracT = 0.5);
  declareProperty( "MaxOverlapFracTT", m_maxOverlapFracTT = 0.35); // essentially: max 1 common hit
  declareProperty( "InitTrackStates", m_initTrackStates = true );
  declareProperty( "FitTracks", m_fitTracks = true );
  declareProperty( "StateInitTool", m_stateinittool );
  declareProperty( "Fitter", m_fitter );
  declareProperty( "MaxChi2DoF", m_maxChi2DoF = 3 );
  declareProperty( "MaxChi2DoFVelo", m_maxChi2DoFVelo = 999 );
  declareProperty( "MaxChi2DoFT", m_maxChi2DoFT = 999 );
  declareProperty( "MaxChi2DoFMatchTT", m_maxChi2DoFMatchAndTT = 999 );
  declareProperty( "MinLongLongDeltaQoP", m_minLongLongDeltaQoP = -1 );
  declareProperty( "MinLongDownstreamDeltaQoP", m_minLongDownstreamDeltaQoP = 5e-6 );
  declareProperty( "MaxGhostProb", m_maxGhostProb = 99999); // The default value is 999
  declareProperty( "UseAncestorInfo", m_useAncestorInfo = true);
  declareProperty( "DoNotRefit", m_doNotRefit = false);
  declareProperty( "AddGhostProb", m_addGhostProb = false);
  declareProperty( "GhostIdTool", m_ghostTool );
  declareProperty( "SplitByType", m_splitByType = false);
  declareProperty( "FillRecTrackSummary", m_fillRecTrackSummary = false);
  declareProperty( "TracksOutContainers", m_tracksOutContainers );
  declareProperty( "BatchSize", m_batchSize = 50);
  m_supportedTrackTypes = { 1, 3, 4, 5, 6}; //Velo, Long, Upstream, Downstream, TTrack
  for ( auto type : m_supportedTrackTypes ){
    m_tracksOutContainers[LHCb::Track::TypesToString(type)] = "";
  }
}


//=============================================================================
// Initialization
//=============================================================================
StatusCode TrackBestTrackCreator::initialize()
{
#if DEBUGHISTOGRAMS
  StatusCode sc = GaudiHistoAlg::initialize();
#else
  StatusCode sc = GaudiAlgorithm::initialize();
#endif
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  if( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;

  // ------------------------------
  if( m_initTrackStates ) {
    sc = m_stateinittool.retrieve();
    if ( sc.isFailure() ) return sc;
  }
  if( m_fitTracks ) {
    sc = m_fitter.retrieve();
    if ( sc.isFailure() ) return sc;
  }
  if (m_addGhostProb){
    sc =  m_ghostTool.retrieve();
    if ( sc.isFailure() ) return sc;
  }

  if (m_splitByType){
    for (auto it :  m_tracksOutContainers){
      if( msgLevel(MSG::DEBUG) ){
        debug()<<"Splitting types up into:"<<endmsg;
        debug()<<"  "<<it.first<<" -> "<<it.second<<endmsg;
      }
      if (std::none_of(m_supportedTrackTypes.begin(),m_supportedTrackTypes.end(),
                       [it](int i){return it.first==LHCb::Track::TypesToString(i);})){
        warning()<<it.first << " does not correspond to a supported track type."<<endmsg;
      }
    }
  }

  // Process in batches in case we are not using MasterFitter
  std::string prefix ("TrackMasterFitter");
  m_processInBatches = m_fitter.typeAndName().compare(0, prefix.size(), prefix);

  // Print out the user-defined settings
  // -----------------------------------
  if( msgLevel(MSG::DEBUG) )
    debug() << endmsg
	    << "============ TrackBestTrackCreator Settings ===========" << endmsg
	    << "TracksInContainers : " << getProperty( "TracksInContainers" ).toString()                    << endmsg
	    << "TrackOutContainer  : " << m_tracksOutContainer                                              << endmsg
	    << "=======================================================" << endmsg
	    << endmsg;

  return sc;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode TrackBestTrackCreator::finalize()
{
  if( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;

  if( m_fitTracks ) {
    double perf = (1.0 - counter("FitFailed").mean())*100;
    info() << "  Fitting performance   : "
           << format( " %7.2f %%", perf ) << endmsg;
  }

  m_stateinittool.release().ignore();
  m_fitter.release().ignore();

#if DEBUGHISTOGRAMS
  return GaudiHistoAlg::finalize();
#else
  return GaudiAlgorithm::finalize();
#endif
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode TrackBestTrackCreator::execute()
{
  if ( msgLevel(MSG::DEBUG) ) {
    debug() << "==> Execute" << endmsg
      << "Add ghost prob " << m_addGhostProb << endmsg;
  }

  if (m_addGhostProb){
    m_ghostTool->beginEvent().ignore();
  }
  m_nGhosts = 0;

  // create pool for TrackData objects for all input tracks
  std::vector<TrackData> trackdatapool;
  // keep a record of which track goes where
  CopyMap copymap;
  // get total number of tracks
  size_t nTracks = std::accumulate(std::begin(m_tracksInContainers), std::end(m_tracksInContainers),
                                   size_t(0), [this] (size_t sz, const std::string& loc)
                                   { return sz + get<LHCb::Track::Range>(loc).size(); });
  // reserve enough space so we don't have to reallocate
  trackdatapool.reserve(nTracks);
  copymap.reserve(nTracks);
  try {
    // generate the TrackData objects for the input tracks, initialising the
    // States for use in the Kalman filter on the way
    TrackDataGen gen(*this, copymap, m_fitter.typeAndName());
    std::generate_n(std::back_inserter(trackdatapool), nTracks, gen);
  } catch (const TrackDataGen::DataExhaustedException&) {
    // nothing to do - occasionally running out of tracks to convert is expected
  }

  // take a vector of "references" which is much easier to sort (because less
  // data is moved around)
  std::vector<std::reference_wrapper<TrackData>> alltracks (trackdatapool.begin(), trackdatapool.end());
  
  // sort them by quality
  auto qualitySort = [] (const TrackData& t1, const TrackData& t2) { return t1 < t2; };
  std::stable_sort(alltracks.begin(), alltracks.end(), qualitySort);
  
  // Prepare TrackData reference containers
  std::vector<std::reference_wrapper<TrackData>> successful_tracks;

  // Helper function to verify if t is a clone
  // Conditions for being a clone:
  // * cloneFlag is true
  // * It's a clone of any track in v
  auto isClone = [&] (
    TrackData& t,
    const std::vector<std::reference_wrapper<TrackData>>& v
  ) {
    const auto firstClone = std::find_if(v.begin(), v.end(),
      [&] (const TrackData& t2) { return areClones(t, t2); });
    if (firstClone != v.end()) return true;
    return false;
  };

  if (!m_processInBatches) {
    // Sequential treatment
    std::for_each(alltracks.begin(), alltracks.end(), [&] (TrackData& t) {
      if (
        !t.cloneFlag()
        && !isClone(t, successful_tracks)
      ) {
        std::vector<std::reference_wrapper<TrackData>> to_process {t};
        fitAndSelect(to_process.begin(), to_process.end());
        if (t.isAccepted()) {
          successful_tracks.push_back(t);
        }
      }
    });
  } else {
    // isClone function with added functionality
    auto isCloneExtended = [&] (
      TrackData& t,
      const std::vector<std::reference_wrapper<TrackData>>& v
    ) {
      const auto firstClone = std::find_if(v.begin(), v.end(),
        [&] (const TrackData& t2) { return areClones(t, t2); });
      if (firstClone != v.end()) {
        // Add the clone found to the list of clones of that track
        TrackData& foundT = *firstClone;
        foundT.clones.push_back(t);

        return true;
      }
      return false;
    };

    std::vector<std::reference_wrapper<TrackData>> to_process;
    std::vector<std::reference_wrapper<TrackData>> unsuccessful_tracks;

    RANGES_FOR(auto chunk, ranges::v3::view::chunk(alltracks, m_batchSize)) {
      // Fill to_process
      std::for_each(chunk.begin(), chunk.end(), [&] (TrackData& t) {
        if (
          !t.cloneFlag()
          && !isClone(t, successful_tracks)
          && !isCloneExtended(t, to_process)
        ) {
          to_process.push_back(t);
        }
      });

      // Perform fit
      fitAndSelect(to_process.begin(), to_process.end());

      // Move to successful
      std::partition_copy(to_process.begin(), to_process.end(),
        std::back_inserter(successful_tracks), std::back_inserter(unsuccessful_tracks),
        [] (const TrackData& t) { return t.isAccepted(); }
      );

      // Create new to_process batch
      to_process.clear();
      std::for_each(unsuccessful_tracks.begin(), unsuccessful_tracks.end(), [&] (TrackData& t) {
        std::copy_if(t.clones.begin(), t.clones.end(), std::back_inserter(to_process),
          [&] (TrackData& c) {
            return !isClone(c, successful_tracks) && !isCloneExtended(c, to_process);
          }
        );
      });

      unsuccessful_tracks.clear();
    }

    // Remaining tracks
    while (!to_process.empty()) {
      // Perform fit
      fitAndSelect(to_process.begin(), to_process.end());

      // Move to successful
      std::partition_copy(to_process.begin(), to_process.end(),
        std::back_inserter(successful_tracks), std::back_inserter(unsuccessful_tracks),
        [] (const TrackData& t) { return t.isAccepted(); }
      );

      // Create new to_process batch
      to_process.clear();
      std::for_each(unsuccessful_tracks.begin(), unsuccessful_tracks.end(), [&] (TrackData& t) {
        std::copy_if(t.clones.begin(), t.clones.end(), std::back_inserter(to_process),
          [&] (TrackData& c) {
            return !isClone(c, successful_tracks) && !isCloneExtended(c, to_process);
          }
        );
      });

      unsuccessful_tracks.clear();
    }
  }

  if (!m_splitByType){
    // create output container, and put selected tracks there
    LHCb::Tracks* tracksOutCont = new LHCb::Tracks();
    put(tracksOutCont, m_tracksOutContainer);
    // insert selected tracks
    tracksOutCont->reserve(successful_tracks.size());
    for (TrackData& tr : successful_tracks) {
      // make tr release ownership of track
      tracksOutCont->add(tr.trackptr().release());
    }
  }else{
    /// create mapping, LHCb::Track::Types::Ttrack has the enum 7
    std::unordered_map<int,LHCb::Tracks*> tracksOutContsMap;
    for (auto it : m_tracksOutContainers ){
      auto type = LHCb::Track::TypesToType(it.first);
      if (it.second!="" && type!=LHCb::Track::Types::TypeUnknown){
        tracksOutContsMap[type] = new LHCb::Tracks();
        put( tracksOutContsMap[type], it.second);
      }
    }
    for (TrackData& tr: successful_tracks) {
      // make tr release ownership of track
      auto container = tracksOutContsMap.find(tr.track().type());
      if ( container != tracksOutContsMap.end())
        container->second->add(tr.trackptr().release());
      else{
        if (msgLevel(MSG::DEBUG)) {
          debug() << "No outputlocation for track type "<< tr.track().type()<<" was specified!" << endmsg;
        }
      }
    }
  }

  if( m_fillRecTrackSummary) fillRecTrackSummary();

  if (msgLevel(MSG::DEBUG)) {
    debug() << "Selected " << successful_tracks.size() << " out of "
            << nTracks << " tracks. Rejected " << m_nGhosts << endmsg;
  }
  return StatusCode::SUCCESS;
}

void TrackBestTrackCreator::fitAndSelect (
  const std::vector<std::reference_wrapper<TrackData>>::iterator& tracksBegin,
  const std::vector<std::reference_wrapper<TrackData>>::iterator& tracksEnd
) const {
  if (m_fitTracks) {
    fitAndUpdateCounters(tracksBegin, tracksEnd);

    unsigned i = 0;
    std::for_each(tracksBegin, tracksEnd, [&] (TrackData& trackData) {
      LHCb::Track& track = trackData.track();
      trackData.setAccepted(track.fitStatus() == LHCb::Track::FitStatus::Fitted);

      // Impose tighter restrictions to composite tracks
      if (trackData.isAccepted() && (
        track.type() == LHCb::Track::Long ||
        track.type() == LHCb::Track::Upstream ||
        track.type() == LHCb::Track::Downstream)) {

        auto* fitResult = track.fitResult();
        const LHCb::ChiSquare& chi2 = fitResult->chi2();
        const LHCb::ChiSquare& chi2T = fitResult->chi2Downstream();
        const LHCb::ChiSquare& chi2Velo = fitResult->chi2Velo();
        
        // note: this includes TT hit contribution
        const LHCb::ChiSquare chi2MatchAndTT = chi2 - chi2T - chi2Velo;
        
        if (msgLevel(MSG::DEBUG)) {
          debug() << "Track #" << i << " "
            << chi2.chi2() << " " << chi2.nDoF() << " (" << (chi2.chi2PerDoF() < m_maxChi2DoF ? "1" : "0") << "), "
            << chi2T.chi2() << " " << chi2T.nDoF() << " (" << (chi2T.chi2PerDoF() < m_maxChi2DoFT ? "1" : "0") << "), "
            << chi2Velo.chi2() << " " << chi2Velo.nDoF() << " (" << (chi2Velo.chi2PerDoF() < m_maxChi2DoFVelo ? "1" : "0") << "), "
            << chi2MatchAndTT.chi2() << " " << chi2MatchAndTT.nDoF() << " (" << (chi2MatchAndTT.chi2PerDoF() < m_maxChi2DoFMatchAndTT ? "1" : "0") << "), "
            << track.ghostProbability() << " (" << (track.ghostProbability() < m_maxGhostProb ? "1" : "0") << ") " << endmsg;
        }

        trackData.setAccepted (
          chi2.chi2PerDoF() < m_maxChi2DoF &&
          chi2T.chi2PerDoF() < m_maxChi2DoFT &&
          chi2Velo.chi2PerDoF() < m_maxChi2DoFVelo &&
          chi2MatchAndTT.chi2PerDoF() < m_maxChi2DoFMatchAndTT &&
          (!m_addGhostProb || track.ghostProbability() < m_maxGhostProb));

        if (!trackData.isAccepted()) {
          ++m_nGhosts;
        }
      }

      if (msgLevel(MSG::DEBUG)) {
        debug() << "Track #" << i++ << (trackData.isAccepted() ? " accepted" : " not accepted") << endmsg;
      }
    });
  }
}

/**
 * @brief      Invokes the fitter and sets some counters.
 */
void TrackBestTrackCreator::fitAndUpdateCounters (
  const std::vector<std::reference_wrapper<TrackData>>::iterator& tracksBegin,
  const std::vector<std::reference_wrapper<TrackData>>::iterator& tracksEnd
) const {
  std::vector<std::reference_wrapper<LHCb::Track>> fittingTracks;

  std::for_each(tracksBegin, tracksEnd, [&] (TrackData& trackData) {
    LHCb::Track& track = trackData.track();

    // Conditions for fitting a track
    if ((!m_doNotRefit || (track.fitStatus() != LHCb::Track::FitStatus::Fitted && track.fitStatus() != LHCb::Track::FitStatus::FitFailed))
        && (track.nStates()!=0 && !track.checkFlag(LHCb::Track::Flags::Invalid))) {
      fittingTracks.push_back(track);
    }
  });

  m_fitter->operator()(fittingTracks);

  // Sequential treatment
  std::for_each(tracksBegin, tracksEnd, [&] (TrackData& trackData) {
    LHCb::Track& track = trackData.track();

    bool badinput  = false;
    bool fitfailed = false;
    /// for counters
    std::string prefix = Gaudi::Utils::toString(track.type());
    if (track.checkFlag(LHCb::Track::Backward)) prefix += "Backward";
    prefix += '.';
    
    if (m_doNotRefit && (trackData.previousStatus() == LHCb::Track::FitStatus::Fitted
                         || trackData.previousStatus() == LHCb::Track::FitStatus::FitFailed) ){
      if (trackData.previousStatus() == LHCb::Track::FitStatus::Fitted){
        counter("FittedBefore") += 1;
        if (m_addGhostProb) m_ghostTool->execute(track).ignore();
      }
      else {
        /// fit failed before
        /// This should always be 0 as this type is filtered out when initializing the tracks
        counter("FitFailedBefore") += 1;
        track.setFlag( LHCb::Track::Flags::Invalid, true );
      }
    } else {
      if( track.nStates()==0 ||
          track.checkFlag( LHCb::Track::Flags::Invalid ) )  {
        track.setFlag( LHCb::Track::Flags::Invalid, true );
        badinput = true;
      } else {
        // Note: The track has already been fitted
        if (track.fitStatus() == LHCb::Track::FitStatus::Fitted) {
          if (m_addGhostProb) m_ghostTool->execute(track).ignore();
          // Update counters
          if (track.nDoF() > 0) {
            double chisqprob = track.probChi2();
            counter(prefix + "chisqprobSum") += chisqprob;
            counter(prefix + "badChisq") += bool(chisqprob<0.01);
          }
          counter(prefix + "flipCharge") += bool(trackData.qOverP() * track.firstState().qOverP() < 0);
          counter(prefix + "numOutliers") += track.nMeasurementsRemoved();
        } else {
          track.setFlag(LHCb::Track::Flags::Invalid, true);
          fitfailed = true;
          counter(prefix + "FitFailed") += int(fitfailed);
        }
      }
    }
    // stick as close as possible to whatever EventFitter prints
    counter("BadInput")  += int(badinput);
    counter("FitFailed") += int(fitfailed);
  });
}

bool TrackBestTrackCreator::veloOrClones(const TrackData& lhs,
                                         const TrackData& rhs) const
{
  const double fR   = lhs.overlapFraction(rhs, TrackData::VeloR);
#ifndef DEBUGHISTOGRAMS
  if (fR > m_maxOverlapFracVelo) return true;
#endif
  const double fPhi = lhs.overlapFraction(rhs, TrackData::VeloPhi);
#ifdef DEBUGHISTOGRAMS
  if(fR>0) plot1D(fR,"veloROverlapFractionH1",0,1);
  if(fPhi>0) plot1D(fPhi,"veloPhiOverlapFractionH1",0,1);
  return (fR > m_maxOverlapFracVelo) || (fPhi > m_maxOverlapFracVelo);
#else
  return fPhi > m_maxOverlapFracVelo;
#endif
}

bool TrackBestTrackCreator::TClones(const TrackData& lhs,
                                    const TrackData& rhs) const
{
  const double f = lhs.overlapFraction(rhs,TrackData::T);
#ifdef DEBUGHISTOGRAMS
  if(f>0) plot1D(f,"TOverlapFractionH1",0,1);
#endif
  return f > m_maxOverlapFracT;
}

bool TrackBestTrackCreator::TTClones(const TrackData& lhs,
                                     const TrackData& rhs) const
{
  const double f = lhs.overlapFraction(rhs,TrackData::TT);
#ifdef DEBUGHISTOGRAMS
  if(f>0) plot1D(f,"TTOverlapFractionH1",0,1);
#endif
  return f > m_maxOverlapFracTT;
}

bool TrackBestTrackCreator::areClones(const TrackData& it, const TrackData& jt)
{
  const LHCb::Track &itrack(it.track()), &jtrack(jt.track());
  const int itype(itrack.type()), jtype(jtrack.type());
  const double dqop = it.qOverP() - jt.qOverP();
  const int offset = 256;
  switch (itype + offset * jtype) {
  case LHCb::Track::Types::Long + offset * LHCb::Track::Types::Long:
#ifdef DEBUGHISTOGRAMS
    if (TClones(it, jt) && veloOrClones(it, jt)) {
      plot(dqop, "LLDqopClones", -1e-5, 1e-5);
    } else if (TClones(it, jt)) {
      plot(dqop, "LLDqopTClones", -1e-5, 1e-5);
    } else if (veloOrClones(it, jt)) {
      plot(dqop, "LLDqopVeloOrClones", -1e-5, 1e-5);
    }
#endif
    return TClones(it, jt) &&
      (std::abs(dqop) < m_minLongLongDeltaQoP || veloOrClones(it, jt));
  case LHCb::Track::Types::Long + offset*LHCb::Track::Types::Downstream:
  case LHCb::Track::Types::Downstream + offset*LHCb::Track::Types::Long:
#ifdef DEBUGHISTOGRAMS
    if (TClones(it, jt)) {
      plot(dqop, "DLDqop", -2e-5, 2e-5);
      if (TTClones(it, jt))
        plot(dqop, "DLDqopTTClones", -2e-5, 2e-5);
    }
#endif
    return TClones(it, jt) &&
      (std::abs(dqop) < m_minLongDownstreamDeltaQoP || TTClones(it, jt));
  case LHCb::Track::Types::Downstream + offset*LHCb::Track::Types::Downstream:
    // it seems that there are no down stream tracks that share T hits ...
#ifdef DEBUGHISTOGRAMS
    if(TClones(it, jt)) {
      plot(dqop, "DDDqop", -1e-4, 1e-4);
    }
#endif
    return TClones(it, jt) && TTClones(it, jt);
  case LHCb::Track::Types::Long + offset*LHCb::Track::Types::Upstream:
  case LHCb::Track::Types::Upstream + offset*LHCb::Track::Types::Long:
  case LHCb::Track::Types::Upstream + offset*LHCb::Track::Types::Upstream:
    return veloOrClones(it, jt) && TTClones(it, jt);
  case LHCb::Track::Types::Long + offset*LHCb::Track::Types::Velo:
  case LHCb::Track::Types::Velo + offset*LHCb::Track::Types::Long:
  case LHCb::Track::Types::Upstream + offset*LHCb::Track::Types::Velo:
  case LHCb::Track::Types::Velo + offset*LHCb::Track::Types::Upstream:
  case LHCb::Track::Types::Velo + offset*LHCb::Track::Types::Velo:
    return veloOrClones(it, jt);
  case LHCb::Track::Types::Long + offset*LHCb::Track::Types::Ttrack:
  case LHCb::Track::Types::Ttrack + offset*LHCb::Track::Types::Long:
  case LHCb::Track::Types::Downstream + offset*LHCb::Track::Types::Ttrack:
  case LHCb::Track::Types::Ttrack + offset*LHCb::Track::Types::Downstream:
  case LHCb::Track::Types::Ttrack + offset*LHCb::Track::Types::Ttrack:
    return TClones(it, jt);
  case LHCb::Track::Types::Ttrack + offset*LHCb::Track::Types::Upstream:
  case LHCb::Track::Types::Upstream + offset*LHCb::Track::Types::Ttrack:
  case LHCb::Track::Types::Ttrack + offset*LHCb::Track::Types::Velo:
  case LHCb::Track::Types::Velo + offset*LHCb::Track::Types::Ttrack:
  case LHCb::Track::Types::Downstream + offset*LHCb::Track::Types::Velo:
  case LHCb::Track::Types::Velo + offset*LHCb::Track::Types::Downstream:
  case LHCb::Track::Types::Downstream + offset*LHCb::Track::Types::Upstream:
  case LHCb::Track::Types::Upstream + offset*LHCb::Track::Types::Downstream:
    break;
  default:
    error() << "Don't know how to handle combi: " << itype << " "
            << jtype << endmsg;
  }
  return false;
}

//=============================================================================
// Put number of ghosts (aka tracks rejected by chi2 and ghost prob cuts) on the TES
//=============================================================================
void TrackBestTrackCreator::fillRecTrackSummary(){

  // Create a new summary object and save to the TES
  LHCb::RecSummary* summary = new LHCb::RecSummary();
  put( summary, LHCb::RecSummaryLocation::Track );

  summary->addInfo( LHCb::RecSummary::nGhosts, m_nGhosts );

}
