#ifndef MOVERECVERTEXTRACKS_H
#define MOVERECVERTEXTRACKS_H 1

/** @class MoveRecVertexTracks MoveRecVertexTracks.h
 *
 *  Copy tracks participating in RecVertices to a new container and
 *  update the vertex' track pointers.
 *
 *  @author Rosen Matev
 *  @date   2016-04-16
 */

#include "GaudiAlg/GaudiAlgorithm.h"
#include <string>

class MoveRecVertexTracks: public GaudiAlgorithm {
public:
  /// Standard constructor
  MoveRecVertexTracks(const std::string& name, ISvcLocator* pSvcLocator);

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override; ///< Algorithm execution

private:
  std::string m_vertexLocation;
  std::string m_outputLocation;
};

#endif // MOVERECVERTEXTRACKS_H
