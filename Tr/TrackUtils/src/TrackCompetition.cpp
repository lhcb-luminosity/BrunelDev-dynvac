// track interfaces
#include <numeric>
#include "Event/Track.h"
#include "TrackCompetition.h"

using namespace LHCb;

DECLARE_ALGORITHM_FACTORY( TrackCompetition )

TrackCompetition::TrackCompetition(const std::string& name,
                       ISvcLocator* pSvcLocator):
  GaudiAlgorithm(name, pSvcLocator)
{
  // constructor
  declareProperty( "inputLocation",  m_inputLocation  = TrackLocation::Default );
  declareProperty( "outputLocation",  m_outputLocation  = TrackLocation::Default );
  declareProperty("fracUsed", m_fracUsed = 0.25);

}

StatusCode TrackCompetition::execute(){

  Tracks* inCont = get<Tracks>(m_inputLocation);
  Tracks* outCont = new Tracks(); outCont->reserve(inCont->size());
  put(outCont, m_outputLocation);

  // sort
  std::sort(inCont->begin(),inCont->end(),
            [](const LHCb::Track* lhs, const LHCb::Track* rhs) {
                return std::make_tuple( lhs->nLHCbIDs(), rhs->chi2() ) >
                       std::make_tuple( rhs->nLHCbIDs(), lhs->chi2() );
  } );

  //TODO: maybe a set is better? This is O(N^2)..
  // or keep used sorted (N log N), and use the fact that ids
  // is also sorted so the lookup is done O(N) instead of O(N^2)
  // or use a bloom filter (which would need to be updated...)
  std::vector<LHCb::LHCbID> used; used.reserve(4000);
  for (const auto& t : *inCont) {
    const std::vector<LHCbID>& ids = t->lhcbIDs();
    auto nUsed = std::count_if(ids.begin(), ids.end(),
                               [&](const LHCbID& id) {
      return std::find(used.begin(), used.end(), id) != used.end();
    });

    if (nUsed < m_fracUsed*ids.size()){
      used.insert(used.end(), ids.begin(), ids.end() );
      outCont->insert(t->clone());
    }
  } // for each

  return StatusCode::SUCCESS;
}
