// Include files

// local
#include "ChargedParticlesToTracks.h"
#include "TrackInterfaces/ITrackFitter.h"

#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"

#include <boost/algorithm/string.hpp>

#include <iostream>

//-----------------------------------------------------------------------------
// Implementation file for class : ChargedParticlesToTracks
//
// 2012-10-08 : Frederic Dupertuis
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( ChargedParticlesToTracks )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
ChargedParticlesToTracks::ChargedParticlesToTracks( const std::string& name,
                                          ISvcLocator* pSvcLocator)
  : GaudiHistoAlg ( name , pSvcLocator )
{
  declareProperty( "ParticlesLocations"    , m_partloc =  { "/Event/CharmCompleteEvent/Phys/D2hhCompleteEventPromptD2KPiLine/Particles" } ) ;
  declareProperty( "TracksOutputLocation"  ,
                   m_trackOutputLocation = "/Event/Rec/Track/MyBest" ) ;
  declareProperty( "MassWindow"            , m_masswindow = -1. );
  declareProperty( "MassOffset"            , m_massoffset = 0. );
  declareProperty( "RefitTracks"           , m_refit = false );
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode ChargedParticlesToTracks::initialize() {
  StatusCode sc = GaudiHistoAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;

  m_trackPreFit = tool<ITrackFitter>("TrackMasterFitter","preFit", this);
  m_trackFit = tool<ITrackFitter>("TrackMasterFitter", "fit", this);

  if(fullDetail()){
    std::transform( m_partloc.begin(), m_partloc.end(),
                    std::inserter(m_linesname,m_linesname.end()),
                    [tokens=std::vector<std::string>{}](const std::string& s) mutable {
      tokens.clear();
      boost::split(tokens, s, boost::is_any_of("/"));
      return std::make_pair( s, *std::prev(tokens.end(),2));
    });
  }
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode ChargedParticlesToTracks::execute() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;

  auto trackCont = new LHCb::Track::Container();
  put(trackCont, m_trackOutputLocation);

  auto propertysvc =
    service<LHCb::IParticlePropertySvc>("LHCb::ParticlePropertySvc",true) ;

  LHCb::Particle::ConstVector particles;

  for( const auto& loc : m_partloc ) {
    if(!exist<LHCb::Particle::Range>(loc)) continue;
    auto particlesRange = get<LHCb::Particle::Range>(loc);
    for( const auto& p : particlesRange ) {
      auto prop = propertysvc->find(p->particleID());
      if(m_masswindow<0.||std::abs(p->measuredMass()-(prop->mass()+m_massoffset))<m_masswindow){
        particles.push_back(p);
        if(fullDetail()){
          plot(p->measuredMass(), m_linesname[loc], prop->mass()+m_massoffset-1.5*m_masswindow,
               prop->mass()+m_massoffset+1.5*m_masswindow, (int)(3*m_masswindow));
        }
      }
    }
  }

  if(particles.empty()){
    setFilterPassed(false);
    return StatusCode::SUCCESS;
  }

  DumpTracks(particles,*trackCont);

  return StatusCode::SUCCESS;
}


//=============================================================================

void ChargedParticlesToTracks::DumpTracks(LHCb::Particle::ConstVector particles,
                                          LHCb::Track::Container& trackCont)
{
  for( const auto& p : particles ) {
    if(p->isBasicParticle()){
      auto proto = p->proto();
      if(proto){
        if(proto->charge() != 0){
          auto track = p->proto()->track();
          if(track){
            auto refittedtrack = const_cast<LHCb::Track*>(p->proto()->track());
            if (m_refit) {
              m_trackPreFit->fit(*refittedtrack);
              m_trackFit->fit(*refittedtrack);
            }
            if (find(trackCont.begin(), trackCont.end(), refittedtrack) == trackCont.end()) {
              trackCont.add(refittedtrack);
            }
          }
        }
      }
    }else{
      DumpTracks(p->daughtersVector(), trackCont);
    }
  }
}
