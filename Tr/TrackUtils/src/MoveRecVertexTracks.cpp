#include "Event/RecVertex.h"
#include "Event/Track.h"

#include "MoveRecVertexTracks.h"


DECLARE_ALGORITHM_FACTORY(MoveRecVertexTracks)

MoveRecVertexTracks::MoveRecVertexTracks(const std::string& name, ISvcLocator* pSvcLocator)
  : GaudiAlgorithm(name, pSvcLocator)
{
  declareProperty("VertexLocation",
                  m_vertexLocation = LHCb::RecVertexLocation::Velo3D);
  declareProperty("OutputLocation", m_outputLocation );
}

StatusCode MoveRecVertexTracks::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize();
  if (sc.isFailure()) return sc;

  if (m_outputLocation.empty()) {
    return Error("OutputLocation is not specified");
  }

  return sc;
}

StatusCode MoveRecVertexTracks::execute() {
  auto vertices = getIfExists<LHCb::RecVertices>(m_vertexLocation);
  if (!vertices) {
    return Error("Container " + m_vertexLocation + " does not exist.");
  }

  auto newTracks = new LHCb::Tracks{};
  put(newTracks, m_outputLocation);

  LHCb::RecVertex::TrackWithWeightVector newVertexTracks;

  for (auto vertex: *vertices) {
    auto nTracks = vertex->tracks().size();

    newVertexTracks.clear();
    newVertexTracks.reserve(nTracks);

    for (size_t i = 0; i < nTracks; ++i) {
      auto* newTrack = vertex->tracks()[i]->clone();
      newTracks->insert(newTrack);
      newVertexTracks.emplace_back(newTrack, vertex->weights()[i]);
    }

    vertex->setTracksWithWeights(newVertexTracks);
  }

  return StatusCode::SUCCESS;
}
