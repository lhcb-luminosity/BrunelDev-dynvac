// This File contains the implementation of the TsaEff
// C++ code for 'LHCb Tracking package(s)'
//

// track interfaces
#include "Event/Track.h"

#include "TTrackFromLong.h"
#include "GaudiKernel/SystemOfUnits.h"
using namespace LHCb;
using namespace Gaudi::Units;

DECLARE_ALGORITHM_FACTORY( TTrackFromLong )

TTrackFromLong::TTrackFromLong(const std::string& name,
                       ISvcLocator* pSvcLocator):
  GaudiAlgorithm(name, pSvcLocator)
{
  // constructor
 this->declareProperty("inputLocation", m_inputLocation = TrackLocation::Forward);
 this->declareProperty("outputLocation", m_outputLocation = TrackLocation::Seed);
}

StatusCode TTrackFromLong::execute(){

  Tracks* trackCont = get<Tracks>(m_inputLocation);
  Tracks* newCont = new Tracks();
  newCont->reserve(trackCont->size());

  // loop
  for (Tracks::const_iterator iterT = trackCont->begin(); iterT != trackCont->end(); ++iterT){
    Track* aTrack = convert(*iterT);
    if (aTrack->nLHCbIDs()>4){ newCont->insert(aTrack);} else {delete aTrack;}
  } // iterT


  put(newCont,m_outputLocation);

  return StatusCode::SUCCESS;
}

Track* TTrackFromLong::convert(const Track* aTrack) const{

  Track* tSeed = new Track();
  tSeed->setType( LHCb::Track::Types::Ttrack );
  tSeed->setHistory( LHCb::Track::History::PatSeeding);

  const State& lastState =  aTrack->closestState(9000.*mm);

  LHCb::State tState;
  tState.setLocation( LHCb::State::AtT );
  tState.setState(lastState.stateVector());
  tState.setZ(lastState.z());

  tState.setCovariance(lastState.covariance());
  tSeed->addToStates(tState);
  tSeed->setPatRecStatus(Track::PatRecStatus::PatRecIDs);

  for ( const auto& id : aTrack->lhcbIDs() ) {
    if (id.isOT() || id.isIT()) tSeed->addToLhcbIDs(id);
  } // iter

  return tSeed;
}
