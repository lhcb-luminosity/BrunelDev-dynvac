/** @class VertexListFilter VertexListFilter.h
 *
 *  Algorithm to filter events in which a track list is not empty
 *
 *  @author W. Hulsbergen
 *  @date   2008
 */

// Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#include <string>

// track interfaces
#include "Event/RecVertex.h"

class VertexListFilter final : public GaudiAlgorithm
{
public:
  using GaudiAlgorithm::GaudiAlgorithm;
  StatusCode execute() override;

private:
  Gaudi::Property<std::string> m_inputLocation { this, "InputLocation", LHCb::RecVertexLocation::Primary } ;
};


DECLARE_ALGORITHM_FACTORY( VertexListFilter )

StatusCode VertexListFilter::execute()
{
  LHCb::RecVertex::Range vertices = get<LHCb::RecVertex::Range>(m_inputLocation);
  setFilterPassed( !vertices.empty() ) ;
  return  StatusCode::SUCCESS ;
}
