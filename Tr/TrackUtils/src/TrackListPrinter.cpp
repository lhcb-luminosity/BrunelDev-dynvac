// track interfaces
#include "Event/Track.h"

/** @class TrackListPrinter TrackListPrinter.h
 *
 *  @author M.Needham
 *  @date   30/05/2006
 */

#include "GaudiAlg/GaudiAlgorithm.h"
#include <string>

class TrackListPrinter final : public GaudiAlgorithm {
public:

  // Constructors and destructor
  TrackListPrinter(const std::string& name,
                   ISvcLocator* pSvcLocator);

  StatusCode execute() override;

private:
  std::string m_inputLocation;
};

DECLARE_ALGORITHM_FACTORY( TrackListPrinter )

TrackListPrinter::TrackListPrinter(const std::string& name,
                                   ISvcLocator* pSvcLocator)
: GaudiAlgorithm(name, pSvcLocator)
{
  // constructor
  declareProperty( "InputLocation",  m_inputLocation  = LHCb::TrackLocation::Default );
}

StatusCode TrackListPrinter::execute()
{
  for ( const auto& t : get<LHCb::Track::Range>(m_inputLocation) ) info() << *t << endmsg;
  return StatusCode::SUCCESS;
}

