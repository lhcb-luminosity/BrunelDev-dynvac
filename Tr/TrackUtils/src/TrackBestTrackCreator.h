#pragma once

// Include files
// -------------
// from STD
#include <algorithm>
#include <exception>
#include <unordered_map>
#include <array>
#include <memory>
#include <cassert>
#include <type_traits>
#include <cstddef>

// TrackEvent
#include "Event/Track.h"
#include "Event/Node.h"
#include "Event/TrackFunctor.h"
#include "Event/KalmanFitResult.h"
#include "Event/RecSummary.h"

// LHCbKernel
#include "Kernel/HitPattern.h"
#include "Kernel/LHCbID.h"

// Range v3
#include <range/v3/view.hpp>
#include <range/v3/range_for.hpp>

// local

// Include files
// -------------
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#if DEBUGHISTOGRAMS
#include "GaudiAlg/GaudiHistoAlg.h"
#endif
#include "GaudiKernel/ToolHandle.h"

// from TrackInterfaces
#include "TrackInterfaces/ITrackCloneFinder.h"
#include "TrackInterfaces/ITrackFitter.h"
#include "TrackInterfaces/ITrackStateInit.h"
#include "TrackInterfaces/IGhostProbability.h"

#include "TrackVectorFit/Types.h"
#include "TrackKernel/TrackCloneData.h"

namespace  {

  /// structure to save some data for each track
  class TrackData : public LHCb::TrackCloneData<>
  {
  private:
    bool m_isAccepted;
    double m_qOverP;
    LHCb::Track::FitStatus m_fitStatus;
    enum { Clone = 1 };

  public:
    /// constructor
    TrackData (std::unique_ptr<LHCb::Track> tr) :
      TrackCloneData<>(std::move(tr)),
      m_qOverP(track().firstState().qOverP()),
      m_fitStatus(track().fitStatus())
    { }
    /// return q/p (or what it was at construction time)
    double qOverP () const { return m_qOverP; }
    LHCb::Track::FitStatus previousStatus () const { return m_fitStatus; }
    bool cloneFlag () const { return userFlags() & Clone; }
    void setCloneFlag () { setUserFlags(userFlags() | Clone); }
    bool isAccepted () const { return m_isAccepted; }
    void setAccepted (const bool& isAccepted) { m_isAccepted = isAccepted; }

    std::vector<std::reference_wrapper<TrackData>> clones;
  };
}

/** @brief kill clones among tracks in input container and Kalman-fit survivors
 *
 * @author Wouter Hulsbergen
 * - initial release
 *
 * @author Manuel Schiller
 * @date 2014-11-18
 * - simplify, C++11 cleanups, use BloomFilter based TrackCloneData
 * @date 2014-12-09
 * - add code to use ancestor information on tracks to deal with obvious clones
 */
class TrackBestTrackCreator final :
#if DEBUGHISTOGRAMS
  public GaudiHistoAlg
#else
  public GaudiAlgorithm
#endif
{
public:
  /// Standard constructor
  TrackBestTrackCreator(const std::string& name, ISvcLocator* pSvcLocator);

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization

private:
  ToolHandle<ITrackStateInit> m_stateinittool;
  ToolHandle<ITrackFitter> m_fitter;
  ToolHandle<IGhostProbability> m_ghostTool;

  void fillRecTrackSummary();


  // job options
  // -----------
  // input Track container paths
  std::vector<std::string> m_tracksInContainers;
  // output Track container path
  std::string m_tracksOutContainer;
  double m_maxOverlapFracVelo;
  double m_maxOverlapFracT;
  double m_maxOverlapFracTT;
  double m_minLongLongDeltaQoP;
  double m_minLongDownstreamDeltaQoP;
  double m_maxChi2DoF;
  double m_maxChi2DoFVelo;
  double m_maxChi2DoFT;
  double m_maxChi2DoFMatchAndTT;
  double m_maxGhostProb;
  /// fit the tracks using the Kalman filter?
  bool m_fitTracks;
  /// initialise track states using m_stateinittool?
  bool m_initTrackStates;
  /// Add the Ghost Probability to a track
  bool m_addGhostProb;
  /// use ancestor information to identify obvious clones
  bool m_useAncestorInfo;
  /// Do not refit already fitted tracks
  bool m_doNotRefit;
  /// Put final tracks into separate containers according to track type
  bool m_splitByType;
  /// Fill the RecSummary object for information about number of rejected 'ghost' tracks
  bool m_fillRecTrackSummary;
  std::map<std::string,std::string> m_tracksOutContainers;
  std::vector<int> m_supportedTrackTypes;
  /// Process sequentially or in batches
  bool m_processInBatches;
  unsigned m_batchSize;

  mutable int m_nGhosts;

protected:
  /// are tracks clones in VeloR and VeloPhi
  bool veloClones (const TrackData&, const TrackData&) const;
  /// are tracks clones in VeloR or VeloPhi
  bool veloOrClones (const TrackData&, const TrackData&) const;
  /// are tracks clones in T
  bool TClones (const TrackData&, const TrackData&) const;
  /// are tracks clones in TT
  bool TTClones (const TrackData&, const TrackData&) const;

  void fitAndSelect (
    const std::vector<std::reference_wrapper<TrackData>>::iterator& tracksBegin,
    const std::vector<std::reference_wrapper<TrackData>>::iterator& tracksEnd
  ) const;

  void fitAndUpdateCounters (
    const std::vector<std::reference_wrapper<TrackData>>::iterator& tracksBegin,
    const std::vector<std::reference_wrapper<TrackData>>::iterator& tracksEnd
  ) const;

  /// check if tracks pointed to by their TrackData objects are clones
  bool areClones (const TrackData& it, const TrackData& jt);

  /// mapping between original track and the index of its copy
  typedef std::pair<const LHCb::Track*, size_t> CopyMapEntry;
  typedef std::vector<CopyMapEntry> CopyMap;

  /** @brief "generator" for TrackData objects
   *
   * @author Manuel Schiller
   * @date 2014-11-18
   *
   * goes through track containers, clones the tracks, pre-initialises the
   * Kalman filter fit with the TrackStateInitTool, creates a TrackData object
   * from it, and returns a the newly created object
   *
   * if m_useAncestorInfo is set in the parent, it uses the ancestor field in
   * the track class to flag a large class of obvious clones (e.g. the Velo
   * and T station tracks that got promoted to long); this relies on
   * algorithms properly recording ancestor information, but is highly
   * effective in speeding things up when available
   */
  class TrackDataGen {
  private:
    TrackBestTrackCreator& m_parent; ///< parent TrackBestTrackCreator instance
    decltype(m_tracksInContainers.begin()) m_curcont; ///< next track container
    LHCb::Track::Range m_currange; ///< tracks in current track container
    decltype(m_currange.begin()) m_currangeit; ///< next track

    /// keep track of number of tracks returned
    size_t m_nrtracks;
    /// keep record between original track and copies
    CopyMap& m_copymap;

    std::string m_fitterTypeAndName;

  public:
    /// exception to throw when all tracks have been consumed
    class DataExhaustedException : public std::exception
    {
    private:
      const char* m_what;
    public:
      DataExhaustedException(const char* what) :
        m_what(what) { }
      virtual const char* what() const noexcept override { return m_what; }
    };

    /// constructor
    TrackDataGen(TrackBestTrackCreator& parent, CopyMap& copymap, const std::string& fitterTypeAndName);

    /** @brief destructo
     *
     * flags ancestors as clones if m_useAncestorInfo is set in parent
     */
    ~TrackDataGen();

    /// call to get the next TrackData object
    TrackData operator()();
  };
};
