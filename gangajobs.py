import os
import GangaCore
import utils
from GangaCore.GPI import (
    DiracProxy, GaudiExec, Job, DiracFile, Dirac, SplitByFiles, FileChecker,
    LHCbCompleteChecker)


CREDS = DiracProxy(dirac_env=None, validTime=None, group='lhcb_tape',
                   encodeDefaultProxyFileName=False)

@utils.memoize
def application(datatype):
    TAGS = """
from Configurables import Brunel, CondDB
Brunel().DataType = "{datatype}"
CondDB().LatestGlobalTagByDataType = "{datatype}"
""".strip()
    app = GaudiExec(
        directory=os.path.dirname(__file__),
        options=['run_brunel.py', 'modify_pvs.py', 'add_halfvtx.py'],
        extraOpts=TAGS.format(datatype=datatype),
        getMetadata=True,
    )
    return app


def create_job(job_name, dataset, datatype=None):
    if not datatype:
        metadata = utils.bkMetadata(dataset)
        datatypes = set(str(v['CreationDate'].year) for v in metadata.values())
        assert len(datatypes) == 1
        datatype = datatypes.pop()

    job_params = dict(
        name=job_name,
        application=application(datatype),
        outputfiles=[DiracFile('Brunel.dst'), DiracFile('Brunel-histos.root')],
        inputdata=dataset,
        splitter=SplitByFiles(filesPerJob=1),
        backend=Dirac(
            settings={'CPUTime': (6 * 3600) * 10},  # 6h
            diracOpts="j.setInputDataPolicy('Download')",
            credential_requirements=CREDS,
        ),
        postprocessors=[
            FileChecker(files=['stdout'], searchStrings=['INFO Application Manager Terminated successfully'],
                        failIfFound=False, checkMaster=False),
            LHCbCompleteChecker(),
        ],
    )

    return Job(**job_params)


def save_output_lfns(job, filename=None, ignore_missing=False):
    from gutils.utils import outputfiles
    if not filename:
        filename = 'lfns-{}.txt'.format(job.name)
    files = outputfiles(job, 'Brunel.dst', one_per_job=True, ignore_missing=ignore_missing)
    if os.path.exists(filename):
        raise RuntimeError('File {} already exists'.format(filename))
    with open(filename, 'w') as fout:
        fout.writelines(f.lfn + '\n' for j, f in files)

if __name__ == '__main__':
    ds = utils.get_raw_dataset_runs([199562], ['EXPRESS'])
    create_job('dynvac-rec-199562', ds)
