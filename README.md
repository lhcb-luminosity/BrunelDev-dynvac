Reconstruction for dynamic vacuum studies
===============================================

Developing and testing locally
-----------------------------------------------
```sh
git clone ssh://git@gitlab.cern.ch:7999/lhcb-luminosity/BrunelDev-dynvac.git
cd BrunelDev-dynvac
make install
./run gaudirun.py -o Brunel-199563.opts.py --all-opts run_brunel.py modify_pvs.py add_halfvtx.py input-199563.py | tee 199563.log
```

Running Ganga Jobs
-----------------------------------------------
__Important:__ you must submit from slc6 (e.g. lxplus6), otherwise jobs fail
with the not so helpful `/lib64/libc.so.6: version 'GLIBC_2.14' not found` because apparently cross-compilation does not work.

We submit jobs interactively from `ganga`. Here is an example of some test jobs
```python
import utils
from gangajobs import create_job

# A Local job
mf = MassStorageFile(locations=[
    'root://eoslhcb.cern.ch//eos/lhcb/wg/Luminosity/raw/EXPRESS/195791_0000000129.raw'])
ds = LHCbDataset([mf])
j = create_job('dynvac-rec-test1', ds, '2017')
j.backend = Local()
j.splitter = None
j.submit()

# A Dirac job
ds = utils.get_raw_dataset_runs([199562], ['EXPRESS'])
ds.files = ds.files[26:27]
j = create_job('dynvac-rec-test2', ds)
j.submit()
```

Here are the production jobs (use EXPRESS stream before VELO closing and FULL stream after)
```python
import utils
from gangajobs import create_job

ds_195791 = utils.get_raw_dataset_runs([195791], ['EXPRESS'])
j = create_job('dynvac-rec-195791', ds_195791)
j.submit()

ds_195792 = utils.get_raw_dataset_runs([195792], ['FULL'])
j = create_job('dynvac-rec-195792', ds_195792)
j.submit()

ds_199562 = utils.get_raw_dataset_runs([199562], ['EXPRESS'])
j = create_job('dynvac-rec-199562', ds_199562)
j.submit()

ds_199563 = utils.get_raw_dataset_runs([199563], ['FULL'])
j = create_job('dynvac-rec-199563', ds_199563)
j.submit()

ds_195794 = utils.get_raw_dataset_runs([195794], ['FULL'])
j = create_job('dynvac-rec-195794', ds_195794)
j.submit()

ds_195795 = utils.get_raw_dataset_runs([195795], ['FULL'])
j = create_job('dynvac-rec-195795', ds_195795)
j.submit()

```

### Retrieve output
Output is big, so we obtain a list of LFNs, to be used in other ganga jobs
```python
from gangajobs import save_output_lfns
save_output_lfns(jobs.select(name='dynvac-rec-195791')[-1])
save_output_lfns(jobs.select(name='dynvac-rec-195792')[-1])
save_output_lfns(jobs.select(name='dynvac-rec-199562')[-1])
save_output_lfns(jobs.select(name='dynvac-rec-199563')[-1])
save_output_lfns(jobs.select(name='dynvac-rec-195794')[-1])
save_output_lfns(jobs.select(name='dynvac-rec-195795')[-1])
```
