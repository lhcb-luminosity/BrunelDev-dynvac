import os
import re
import math
import functools
import subprocess
from operator import itemgetter
from itertools import groupby
import rundbapi
try:
    import GangaCore # for Ganga v7 Ganga->GangaCore
except ImportError:
    import Ganga as GangaCore
from GangaCore.GPI import diracAPI, BKQuery, LHCbDataset, MassStorageFile, DiracFile

# Disable the info message from LHCbDataset.bkMetadata()
GangaCore.GPI.config['Logging']['GangaLHCb.Lib.LHCbDataset'] = 'WARNING'
logger = GangaCore.Utility.logging.getLogger('production/utils.py')

STREAM_ID = {
    'FULL': ['90000000', '90000001'],
    'LUMI': ['93000000'],
    'NOBIAS': ['96000000'],
    'BEAMGAS': ['97000000', '97000001'],
    'EXPRESS': ['91000000', '91000001'],
}


def memoize(obj):
    cache = obj.cache = {}

    @functools.wraps(obj)
    def memoizer(*args, **kwargs):
        key = str(args) + str(kwargs)
        if key not in cache:
            cache[key] = obj(*args, **kwargs)
        return cache[key]
    return memoizer


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]


def accumulate(iterable):
    """Poor man's version of itertools.accumulate() from 3.2"""
    total = 0
    for x in iterable:
        total += x
        yield total


def seq_ranges(data):
    ranges = []
    for k, g in groupby(enumerate(sorted(data)), lambda (i, x): i-x):
        groups = map(itemgetter(1), g)
        ranges.append((groups[0], groups[-1]))
    return ranges


def expand_range(x):
    y = x.split(':', 1)
    return range(int(y[0]), int(y[1]) + 1) if len(y) > 1 else [int(x)]


def bkAPI(cmd, timeout=120, tries=3):
    for i in range(tries):
        result = diracAPI('output(dirac.bk.'+cmd+')', timeout=timeout)
        if result != 'Command timed out!':
            break
    if result == 'Command timed out!':
        raise RuntimeError('Command timed out {} times! Increase timeout.'.format(tries))
    if isinstance(result, basestring) or not result.get('OK', False):
        raise RuntimeError('Command failed: ', result)
    return result['Value']


@memoize
def rundb_run_info(run):
    return rundbapi.get_run_info(run)


@memoize
def rundb_fill_info(fill):
    return rundbapi.get_fill_info(fill)


@memoize
def rundb_physics_fills(year):
    start = "{}-01-01T00:00:00".format(year)
    end = "{}-01-01T00:00:00".format(year + 1)
    return rundbapi.get_physics_fills(starttime=start, endtime=end)


@memoize
def rundb_runs_in_fill(fill):
    raise NotImplementedError("rundbapi is missing this entry point")
    url = "http://lbrundb.cern.ch/api/runs_in_fill/{}".format(fill)
    with contextlib.closing(urllib.urlopen(url)) as f:
        if f.code == 404:
            return None
        return json.loads(f.read())


def rundb_is_fill_in_bkk(fill, destinations=['OFFLINE', 'CASTOR']):
    not_in_bkk = [run for run in rundb_runs_in_fill(fill)
                  if run['destination'] in destinations and run['state'] != 'IN BKK']
    if not_in_bkk:
        logger.info('Runs {} are not in the bookkeeping.'.format(
            {run['runid']: run['state'] for run in not_in_bkk}))
    return not not_in_bkk


def _getRunInformation(in_dict):
    return bkAPI("getRunInformation({})".format(repr(in_dict)))


def bkMetadata(dataset):
    if not isinstance(dataset, LHCbDataset):
        raise TypeError('Expect an LHCbDataset object')
    m = dataset.bkMetadata()
    if m['Failed']:
       print m
       raise RuntimeError("bkMetadata call failed")
    return m['Successful']


def get_raw_dataset_runs(runs, streams, warn=True):
    if not runs:
        return LHCbDataset()
    if streams[0] in STREAM_ID:  # expand stream name to numeric id(s)
        streams = STREAM_ID[streams[0]] + streams[1:]

    log = logger.warning if warn else logger.info

    m = {}
    ds = LHCbDataset()
    for subruns in chunks(runs, 20):
        path = '/{}-{}/Real Data/{}/RAW'.format(min(subruns), max(subruns), streams[0])
        logger.info('BK query: {}'.format(path))
        query = BKQuery(dqflag='All', type='Run', path=path)
        ds.files += query.getDataset().files
        m.update(bkMetadata(ds))

    # Select only the files corresponding to requested runs
    ds.files = filter(lambda f: m[f.lfn]['RunNumber'] in runs, ds.files)
    # Check if there are remaining runs and if so use the next stream
    runs_out = set(x['RunNumber'] for x in m.values())
    runs_remain = list(set(runs) - runs_out)
    if runs_remain:
        if len(streams) > 1:
            logger.info('Files for run(s) {} not found in stream {}'.
                        format(runs_remain, streams[0]))
            ds_remain = get_raw_dataset_runs(runs_remain, streams[1:], warn=warn)
            ds.files = ds.files + ds_remain.files
        else:
            log('Files for run(s) {} not found in any of the streams'.format(runs_remain))
    return ds


def get_raw_dataset_fill(fill, streams, destinations=None):
    runs = sorted(bkAPI('getRunsForFill({})'.format(fill)))
    if destinations:
        runs = [run for run in runs if rundb_run_info(run)['destination'] in destinations]
    if not runs:
        return None
    return get_raw_dataset_runs(runs, streams, warn=('FULL' not in streams))


def get_raw_dataset(inp, streams, destinations=None):
    """Return a dataset (list of files) for a run/fill."""
    if inp.isdigit() and int(inp) > 50000:  # this is a run
        return 'run', get_raw_dataset_runs([int(inp)], streams)
    elif inp.isdigit() and int(inp) < 50000:  # this is a fill
        return 'fill', get_raw_dataset_fill(int(inp), streams, destinations)
    else:
        raise ValueError('Unknown input value!')


def _castor_path_to_ganga_file(path, filetype):
    """Return a ganga file object from a CASTOR path."""
    if issubclass(filetype, MassStorageFile):
        # decorate the path with the protocol and svcClass
        return MassStorageFile('mdf:root://castorlhcb.cern.ch/' + path + '?svcClass=lhcbtape')
    elif issubclass(filetype, DiracFile):
        # obtain the corresponding LFN by removing the start of the path
        return DiracFile(lfn=path.replace('/castor/cern.ch/grid', ''))
    else:
        raise NotImplementedError("CASTOR files can only be represented as MassStorageFile or DiracFile")


def castor_listdir(path):
    """Return a list containing the names of the entries in the directory."""
    if not isCastorPath(path):
        return os.listdir(path)
    if not isdir(path):
        raise CastorError("Not a directory: '{}'".format(path))
    stdout, stderr = run('nsls ' + path)
    if stderr:
        raise CastorError(stderr)
    return stdout.splitlines()


def get_raw_castor_dataset_run(run, path, filetype=MassStorageFile):
    """Return a LHCbDataset by listing a directory in CASTOR."""
    rpath = os.path.join(path, str(run))
    filenames = [os.path.join(rpath, bn) for bn in castor_listdir(rpath)]
    files = [_castor_path_to_ganga_file(fn, filetype) for fn in filenames]
    return LHCbDataset(files)


def files_per_job(metadata, max_data_size, mean_n_events):
    """Return the number of files per job given input data and constraints.

    Args:
        metadata (dict): The metadata as returned by bkMetadata(dataset).
        max_data_size (num): Maximum input data per subjobs in bytes.
        mean_n_events (num): Mean number of events processed by each subjobs.

    """
    # How many max. files per subjob can we have, such that the input data
    # size is *always* less than X?
    cumsize = accumulate(
        sorted([v['FileSize'] for v in metadata.itervalues()], reverse=True))
    fpj_maxsize = 1 + max(i for i, v in enumerate(cumsize) if v < max_data_size)

    # How many files per subjob shall we have, in order to have about
    # one subjob per N events on average?
    n_events = sum(v['FullStat'] for v in metadata.values())
    n_subjobs = n_events / mean_n_events
    fpj_events = int(math.ceil(len(metadata) / n_subjobs))

    fpj_ganga = 100  # Ganga is limiting this

    return min(fpj_maxsize, fpj_events, fpj_ganga)


def lbdev_temp(project, version):
    import tempfile
    path = tempfile.mkdtemp(prefix='lbdev-')
    name = '{}Dev_{}'.format(project, version)
    subprocess.check_output(['lb-dev', project + '/' + version,
                             '--dest-dir', path,
                             '--name', name])
    return os.path.join(path, name)


def lb_checkout(app, project, branch, path):
    res = app.execCmd('git lb-use {}'.format(project))
    if res[0]:
        raise RuntimeError('Failed execCmd()\n{}'.format(res))
    res = app.execCmd('git lb-checkout {}/{} {}'.format(project, branch, path))
    if res[0]:
        raise RuntimeError('Failed execCmd()\n{}'.format(res))


def version_tuple(s):
    """Convert v11r2[p3] string to a tuple of integers."""
    return tuple(int(x) for x in re.match('^v(\d+)r(\d+)p?(\d+)?$', s).groups('0'))
