from Configurables import Brunel
from Gaudi.Configuration import appendPostConfigAction

def modify_pvs():
    from Configurables import PatPV3D, PVSeed3DTool, LSAdaptPV3DFitter
    pv = PatPV3D()
    pv.PVOfflineTool.LSAdaptPV3DFitter.TrackErrorScaleFactor = 2.0  # 1.0
    pv.PVOfflineTool.LSAdaptPV3DFitter.Iterations = 50  # 5
    pv.PVOfflineTool.LSAdaptPV3DFitter.maxChi2 = 1600.  # 400.
    pv.PVOfflineTool.addTool(PVSeed3DTool, "PVSeed3DTool")
    pv.PVOfflineTool.PVSeed3DTool.TrackPairMaxDistance = 2.  # 0.3
    pv.PVOfflineTool.PVSeed3DTool.zMaxSpread = 10.  # 3.
    pv.PVOfflineTool.PVSeed3DTool.MinCloseTracks = 1  # 4
appendPostConfigAction(modify_pvs)
