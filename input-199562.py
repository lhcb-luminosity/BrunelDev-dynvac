from Configurables import Brunel, CondDB
from GaudiConf import IOExtension

iox = IOExtension()
iox.inputFiles([
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/Luminosity/raw/EXPRESS/199562_0000000057.raw',
])
Brunel().DataType = "2017"
CondDB().LatestGlobalTagByDataType = "2017"
CondDB().EnableRunStampCheck = False  # needed for EXPRESS
Brunel().EvtMax = 34000  # out of 172606
Brunel().DatasetName = 'Brunel-199562'
