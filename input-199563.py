from Configurables import Brunel, CondDB
from GaudiConf import IOExtension

iox = IOExtension()
iox.inputFiles([
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/Luminosity/raw/BEAMGAS/199563_0000000189.raw',
])
Brunel().DataType = "2017"
CondDB().LatestGlobalTagByDataType = "2017"
Brunel().EvtMax = 2500  # out of 254701
Brunel().EvtMax = 50000  # out of 254701
Brunel().DatasetName = 'Brunel-199563'
