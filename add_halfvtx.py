from Configurables import Brunel
from Configurables import GaudiSequencer
from Configurables import (
    VeloClusterFilter, FastVeloHitManager, FastVeloTracking,
    PatPV3D, PVOfflineTool)
from Configurables import TrackContainerCleaner, SelectTrackInVertex
from Configurables import Gaudi__DataLink as DataLink
from Configurables import OutputStream
from Gaudi.Configuration import appendPostConfigAction


def add_halfvtx():
    halfVertexSeq = GaudiSequencer('HalfVertexSeq', IgnoreFilterPassed=True)
    for side in ['Left', 'Right']:
        cf = VeloClusterFilter(
            side + 'VeloClusterFilter',
            FilterOption=side,
            # InputLiteClusterLocation="Raw/Velo/OriginalLiteClusters",
            # InputClusterLocation="Raw/Velo/OriginalClusters",
            OutputLiteClusterLocation="Raw/Velo/{}LiteClusters".format(side),
            OutputClusterLocation="Raw/Velo/{}Clusters".format(side)
            )
        # The following link is needed because the cluster filter does not
        # copy the 'DecStatus'es, which they are needed for the hit manager.
        dl = DataLink(
            side + "LiteClustersDecStatusLink",
            What=cf.getProp('InputLiteClusterLocation') + "DecStatus",
            Target=cf.OutputLiteClusterLocation + "DecStatus",
            )
        hm = FastVeloHitManager().clone(
            side + 'FastVeloHitManager',
            VeloLiteClusterLocation=cf.OutputLiteClusterLocation
            )
        vt = FastVeloTracking().clone(
            side + 'FastVeloTracking',
            HitManagerName=side + 'FastVeloHitManager',
            OutputTracksName="Rec/Track/{}Velo".format(side)
            )
        pv = PatPV3D().clone(
            side + 'PatPV3D',
            OutputVerticesName="Rec/Vertex/{}Primary".format(side),
            PrimaryVertexLocation="Rec/Vertex/{}PrimaryVertices".format(side),
            )
        pv.addTool(PatPV3D().PVOfflineTool, 'PVOfflineTool')
        pv.PVOfflineTool.InputTracks = [vt.OutputTracksName]
        pv.PVOfflineTool.addTool(PatPV3D().PVOfflineTool.LSAdaptPV3DFitter, 'LSAdaptPV3DFitter')
        pv.PVOfflineTool.LSAdaptPV3DFitter.MinTracks = 2

        # Remove all tracks that don't belong to a PV (to save space on a DST)
        cl = TrackContainerCleaner(side + "PVVeloTracksCleaner")
        cl.inputLocation = vt.OutputTracksName
        cl.selectorName = "SelectTrackInVertex"
        cl.addTool(SelectTrackInVertex, "Selector")
        cl.Selector.VertexContainer = pv.OutputVerticesName

        halfVertexSeq.Members.append(
            GaudiSequencer(side + 'VertexSeq', IgnoreFilterPassed=True,
                           Members=[cf, dl, vt, pv, cl]))

        OutputStream('DstWriter').ItemList += [
            pv.OutputVerticesName + '#1',
            vt.OutputTracksName + '#1',
        ]

    GaudiSequencer('RecoVertexSeq').Members.insert(0, halfVertexSeq)


# def disable_clustering():
#     from Configurables import DecodeVeloRawBuffer, ProcessPhase
#     decoder = DecodeVeloRawBuffer("createBothVeloClusters")
#     decoder.VeloClusterLocation = "Raw/Velo/OriginalClusters"
#     decoder.VeloLiteClustersLocation = "Raw/Velo/OriginalLiteClusters"
#     FastVeloHitManager().VeloLiteClusterLocation = decoder.VeloLiteClustersLocation
#     # print ProcessPhase("Reco")
#     # ProcessPhase('Reco').DetectorList.remove('TrHLT1')
#     # ProcessPhase('Reco').DetectorList.remove('TrHLT2')
#     GaudiSequencer("RecV0MakeV0Seq").Members = []
#     GaudiSequencer("MoniTrSeq").Members = []
#     GaudiSequencer("TrackHLT1FitHLT1Seq").Members = GaudiSequencer("TrackHLT1FitHLT1Seq").Members[:1]

appendPostConfigAction(add_halfvtx)
# appendPostConfigAction(disable_clustering)
