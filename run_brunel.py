"""Run Brunel and produce a DST

./run gaudirun.py run_brunel.py
"""
from Configurables import Brunel, CondDB, TrackSys, RecSysConf, RecMoniConf
from Configurables import MessageSvc
from Gaudi.Configuration import appendPostConfigAction


def modify_tracking():
    """Modify tracking as in the Hlt1BeamGas lines."""
    from Configurables import FastVeloTracking, PatPV3D
    FastVeloTracking().ZVertexMin = -2000
    FastVeloTracking().ZVertexMax = 2000
    PatPV3D().PVOfflineTool.UseBeamSpotRCut = False


def modify_output():
    """Modify output according to what is actually produced."""
    from Configurables import OutputStream
    OutputStream('DstWriter').ItemList = [
        '/Event/Rec/Header#1',
        '/Event/Rec/Status#1',
        '/Event/Rec/Summary#1',
        '/Event/Rec/Track/Best#1',
        '/Event/Rec/Vertex/Primary#1',
        '/Event/Rec/Track/FittedHLT1VeloTracks#1',
        '/Event/DAQ/RawEvent#1',
        ]


# Trim down and modify the reconstruction a bit
Brunel().Detectors = ['Velo', 'PuVeto', 'TT', 'IT', 'OT', 'Magnet', 'Tr']
Brunel().RecoSequence = ["Decoding", "VELO", "TT", "IT", "OT", "TrHLT1", "Vertex", "TrHLT2", "SUMMARY"]
RecMoniConf().MoniSequence = ["VELO", "OT", "ST", "GENERAL", "Tr", "Hlt"]
# don't do the forward so that hits are not lost
TrackSys().TrackPatRecAlgorithms = ['FastVelo', 'PatSeed', 'PatLongLivedTracking']
appendPostConfigAction(modify_tracking)

# Configure output
Brunel().PackType = 'NONE'
Brunel().SplitRawEventOutput = 0.
appendPostConfigAction(modify_output)
# Brunel().OutputType = 'NONE'

# Miscelanea
MessageSvc().Format = '% F%40W%S%7W%R%T %0W%M'
Brunel().PrintFreq = 1000

# Input filters
Brunel().PhysFilterMask = [0, 1 << (36 - 32), 0x80000000]  # HLT2 "content" (95) or EXPRESS (36)
Brunel().Hlt1FilterCode = "HLT_PASS_RE('Hlt1BeamGasBeam1L0.*Decision')"
Brunel().VetoHltErrorEvents = False  # needed for the EXPRESS stream (no Hlt2DecReports, so every event is an "error")
def add_befilter():
    from Configurables import LoKi__ODINFilter as ODINFilter
    from Configurables import GaudiSequencer
    bxfilter = ODINFilter('FilterBX', Code='(ODIN_BXTYP == LHCb.ODIN.Beam1)')
    GaudiSequencer('PhysicsSeq').Members.insert(0, bxfilter)
appendPostConfigAction(add_befilter)

CondDB().EnableRunStampCheck = False  # needed for EXPRESS
